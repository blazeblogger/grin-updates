                  BlazeBlogger - a CMS without boundaries

                            general information


   Copyright (C) 2009-2011 Jaromir Hradilek

   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3 or
   any later version published by the Free Software Foundation;  with no
   Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

   A copy  of the license is included  as a file called FDL  in the main
   directory of the BlazeBlogger source package.


ABSTRACT

This document provides a general information about BlazeBlogger 1.2.0.  For
installation instructions, refer to a file called INSTALL.


TABLE OF CONTENTS

  1. Overview
     1.1. Notable Features
     1.2. Example Usage
  2. Development
  3. Bugs
  4. Copyright


1. OVERVIEW

BlazeBlogger is a simple to use but capable CMS for the command line. Being
written in Perl as a cross-platform application,  and producing static con-
tent without the need of database  servers or server-side scripting,  it is
literally a CMS without  boundaries suitable for a wide variety of web pre-
sentations, from  a personal weblog to a  project page,  or  even a company
website.

1.1 Notable Features

  * Generates valid HTML 4.01 Strict or XHTML 1.1 pages,  and RSS 2.0 feed.
  * Generates yearly and monthly archives, and supports tags.
  * Allows you to create both blog posts and pages.
  * Enables a quick change of theme, style sheet, or localization.
  * Comes with tools for easy management of your blog.

1.2 Example Usage

  ~]$ blaze-init   # Creates a new repository in .blaze/
  ~]$ blaze-add    # Allows you to write a new post in your favorite editor
  ~]$ blaze-make   # Generates a blog from the repository


2. DEVELOPMENT

As a version control system for its source code, BlazeBlogger uses Git with
a public repository located at gitorious.org. To get the latest development
version, use the following command:

  git clone git://gitorious.org/blazeblogger/mainline.git blazeblogger

Alternatively, if you are behind a restrictive firewall,  use the following
command instead:

  git clone http://git.gitorious.org/blazeblogger/mainline.git blazeblogger

If you are planning to make some interesting changes, please, consider clo-
ning the project on Gitorious, so that I can follow you and eventually pull
your work to the upstream.


3. BUGS

To report a bug or to send a patch, please, add a new issue to the bug tra-
cker at <http://code.google.com/p/blazeblogger/issues/>,  or visit the dis-
cussion group at <http://groups.google.com/group/blazeblogger/>.


4. COPYRIGHT

Copyright (C) 2008-2011 Jaromir Hradilek

This program is free software; see the source for copying conditions. It is
distributed  in the hope that it will be useful,  but WITHOUT ANY WARRANTY;
without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PAR-
TICULAR PURPOSE.
